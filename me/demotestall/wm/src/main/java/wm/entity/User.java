package wm.entity;

import lombok.Data;

@Data
public class User {
//  private int id;
//  private String userName;
//  private String password;


//  private 同类；不同类用不了
//  public 公用
//  空白（不写）同包能用
//  protect 继承之后任意可用

  private Integer id;
  private String name;
  private String password;
  private Integer role;
}
