package wm.entity;

import lombok.Data;
import org.apache.ibatis.annotations.Update;

import java.time.DateTimeException;

@Data
public class PersonnelLedger {

  Integer id;
  String name;
  Integer pos;
  String phone_number;
  String Qualification_and_skills;
  DateTimeException update_time;
  Integer state;
}
