package wm.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import wm.entity.User;
import wm.mapper.UserMapper;

import java.util.List;

public interface IUserService {
  PageInfo<User> getPage(Integer pageNum,Integer pageSize);

}
