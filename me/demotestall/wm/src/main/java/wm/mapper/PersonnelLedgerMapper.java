package wm.mapper;

import com.github.pagehelper.PageHelper;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.session.RowBounds;
import org.springframework.core.annotation.Order;
import wm.entity.PersonnelLedger;
import wm.entity.User;

import java.util.List;

@Mapper
public interface PersonnelLedgerMapper {

  //查询语句
  @Select("select * from personnel_ledger")
  List<PersonnelLedger> list();

  @Results({
    @Result(property = "id", column = "id"),//Java属性，数据库表的字段（属性与字段不相同时可用）
    @Result(property = "name", column = "name")
  })
  @Select("select * from personnel_ledger")
  List<PersonnelLedger> listSample();

  @Select("select * from personnel_ledger where id= #{id}")
  User get(@Param("id") Integer id);



  //增加
  @Insert("insert into personnel_ledger(name,pos,phone_number) values(#{name},#{pos},#{phone_number})")
  void addPersonnel_ledger(@Param("name") String name,@Param("pos") Integer pos, @Param("phone_number") String phone_number);

  //修改
  @Update("update personnel_ledger set name = #{name} where id = #{id}")
  int modifyPersonnel_ledger(@Param("id") Integer id,@Param("name") String name);

  //删除
  @Delete("delete from person where id = #{id}")
  int delPersonnel_ledger(@Param("id") Integer id);
}


//分页
//@Override
//public List findAllUser(intNum,int pageSize){
//  //将参数传给这方法
//  //pageNum 当前页， pageSize每一页显示的数据
//  PageHelper.startPage(pageNum,pageSize);
//  return misMypolicylistMapper.selsctByPages();
//
//}
//List selectByPage(RowBounds rowBounds);
//select * from TB_VEHICLE_POLICY order by recid desc


