package wm.mapper;

import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.*;
import wm.entity.User;
import java.util.List;

@Mapper
public interface UserMapper {

  //查询语句
  @Select("select * from user_table")
  List<User> list();

  @Results({
    @Result(property = "id", column = "id"),//Java属性，数据库表的字段（属性与字段不相同时可用）
    @Result(property = "name", column = "name")
  })
  @Select("select * from user_table")
  List<User> listSample();

  @Select("select * from user_table where id= #{id}")
  User get(@Param("id") Integer id);



  //增加
  @Insert("insert into user_table(name,password) values(#{name},#{password})")
  void adduser_table(@Param("name") String name,@Param("password") String password);

  @Insert("insert into user_table(test) values(#{test})")
  void adduser_table_p(@Param("test") Integer test);

  //修改
  @Update("update user_table set name = #{name} where id = #{id}")
  int modifyUser_table(@Param("id") Integer id,@Param("name") String name);

  //删除
  @Delete("delete from user_table where id = #{id}")
  int delUser_table(@Param("id") Integer id);

  PageInfo<User> getPage(Integer pageNum, Integer pageSize);
}
