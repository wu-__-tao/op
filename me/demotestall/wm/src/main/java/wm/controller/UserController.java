package wm.controller;

import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import wm.entity.User;
import wm.mapper.UserMapper;
import wm.service.IUserService;

@RestController
@RequestMapping("/user")
public class UserController {

  @Autowired
  UserMapper userMapper;
//http://localhost:8080/user/adduser
  @RequestMapping("/adduser")
  public String UserController() {
   userMapper.adduser_table("aaa","123456");
   return "ok";
  }

  @GetMapping("getPage")
  public PageInfo<User> getPage(
    @RequestParam(name = "pageNum", defaultValue = "1")Integer pageNum,
    @RequestParam(name = "pageSize", defaultValue = "2")Integer pageSize,
    @RequestParam(name = "username", defaultValue = "")String username
  ){
    return userMapper.getPage(pageNum,pageSize);
  }
}
