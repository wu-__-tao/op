package wm;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import wm.mapper.PersonnelLedgerMapper;
import wm.mapper.UserMapper;

@SpringBootTest
class WmApplicationTests {

  @Autowired
  UserMapper userMapper;

  @Autowired
  PersonnelLedgerMapper personnelLedgerMapper;

  @Test
  public void TestAdd(){
    userMapper.adduser_table("黎俊","123");
//    userMapper.adduser_table_p(0);
//    personnelLedgerMapper.addPersonnel_ledger_p("wt", "1","123546");
//    personnelLedgerMapper.addPersonnel_ledger("wt", 0,"123546");
  }

  @Test
  public void TestSelOne(){
    System.out.println(userMapper.get(1));
  }



  @Test
  public void TestDelete() {
    userMapper.delUser_table(14);
  }

  @Test
  public void TsetUpdata(){
    userMapper.modifyUser_table(2,"黎俊");
  }

}
