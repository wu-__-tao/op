// 后端接口的基本路径
let baseURL = "http://localhost:8090/";
// 在layui中获取jquery对象进行赋值
let $, tree_data, tree_form;


// 构建一棵树
function buildTree(list, parentId) {
    let tree = [];
    for (let i = 0; i < list.length; i++) {
        let node = list[i];
        node.title = node.name;
        node.checked = true;
        node.spread = true;

        if (node.parentsId === parentId) {
            let children = buildTree(list, node.id);
            if (children.length > 0) {
                node.children = children;
            }
            tree.push(node);
        }
    }
    return tree;
}

// 清空数据
function clearData() {
    return new Promise((resolve, reject) => {
        $.post("deleteAll", function (data) {
            resolve(data);
        });
    });
}

// 删除数据,通过id
function delete_id(data) {
    $.ajax({
        url: 'deletemenuById', // 表单提交的接口地址
        type: 'GET', // 请求方式，根据实际情况进行修改
        async: false, // 设置为同步请求
        data: data, // 表单数据
        success: function (res) {
            // 处理成功的回调函数
            layer.closeAll();
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(errorThrown)
            layer.msg(errorThrown)
        }
    });
}

// 修改
function edit_(data) {
    $.ajax({
        url: 'edit', // 表单提交的接口地址
        type: 'POST', // 请求方式，根据实际情况进行修改
        async: false, // 设置为同步请求
        data: data, // 表单数据
        success: function (res) {
            // 处理成功的回调函数
            layer.closeAll();
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(errorThrown)
            layer.msg(errorThrown)
        }
    });
}

// 定义一个查询所有菜单数据的方法，传入jquery对象
function findAll() {
    return new Promise((resolve, reject) => {
        $.getJSON("findall", function (data) {
            // console.log(data)
            tree_data = buildTree(data, null);
            // console.log(tree_data)
            if (tree_data) {
                resolve(tree_data);
            } else {
                reject(new Error('Failed')); // 操作失败时调用 reject 方法
            }
        });
    });
}


// 显示树(操作)
function showTree(layui_tree) {
    findAll().then((tree_data) => {
        //开启节点操作图标
        layui_tree.render({
            elem: "#tree_div",
            data: tree_data,
            edit: ["add", "update", "del"], //操作节点的图标
            click: function (obj) {
                layer.msg(JSON.stringify(obj.data));
            },
            operate: function (obj) {
                let type = obj.type; //得到操作类型：add、edit、del
                let data = obj.data; //得到当前节点的数据
                let elem = obj.elem; //得到当前节点元素

                //操作
                let id = data.id; //得到节点索引
                if (type === 'add') { //增加节点
                    let temphtml = $("#temp").html();
                    layer.open({
                        type: 1
                        , area: ['550px', '300px']
                        , anim: Math.floor(Math.random() * 7)
                        , shade: [0.8, '#393D49'] //显示遮罩
                        , offset: 'auto' //坐标位置
                        , id: 'layerDemo' //防止重复弹出
                        , content: temphtml
                        , btnAlign: 'c' //按钮居中
                    });

                    $(".ppp").show();
                    // 重置表单
                    tree_form.render('select');
                    tree_form.val('example', {
                        "pname": data.name,
                        "parentId": data.id
                    });

                } else if (type === 'update') { //修改节点
                    let edit_obj = {}
                    edit_obj["id"] = data.id
                    edit_obj["parentsId"] = data.parentsId
                    edit_obj["name"] = elem.find('.layui-tree-txt').html()
                    edit_(edit_obj)
                    // console.log(elem.find('.layui-tree-txt').html()); //得到修改后的内容
                    // console.log(data.id)
                } else if (type === 'del') { //删除节点
                    console.log(data.id)
                    let parentsId_ = {}
                    parentsId_["id"] = data.id
                    delete_id(parentsId_)
                }
            }
        });
    }).catch((error) => {
        console.error(error);
    });

}


// 新增一条菜单
function add(data) {
    $.ajax({
        url: 'add', // 表单提交的接口地址
        type: 'POST', // 请求方式，根据实际情况进行修改
        async: false, // 设置为同步请求
        data: data, // 表单数据
        success: function (res) {
            // 处理成功的回调函数
            layer.closeAll();
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(errorThrown)
            layer.msg(errorThrown)
        }
    });
}

// layui引用所有模块
layui.use(function () {
    let layui_tree = layui.tree;
    let layer = layui.layer;
    let util = layui.util;

    tree_form = layui.form;
    // 从layui中取出jquery对象
    $ = layui.jquery;

    // 在任何request发送之前进行拦截过滤
    $.ajaxPrefilter(function (options) {
        // 把基础路径拼接到接口地址中
        options.url = baseURL + encodeURIComponent(options.url);
    });

    // 页面加载完成后立即进行一次菜单数据的查询
    showTree(layui_tree);

    $('.layui-btn-group .layui-btn').on('click', function () {
        let othis = $(this);
        let method = othis.data('method');
        if (method === 'add') {
            let temphtml = $("#temp").html();
            layer.open({
                type: 1
                , area: ['550px', '300px']
                , anim: Math.floor(Math.random() * 7)
                , shade: [0.8, '#393D49'] //显示遮罩
                , offset: 'auto' //坐标位置
                , id: 'layerDemo' //防止重复弹出
                , content: temphtml
                , btnAlign: 'c' //按钮居中
            });

            // 重置表单
            tree_form.render('select');
            $(".ppp").hide();

        } else if (method === 'clear') {
            clearData().then(() => {
                showTree(layui_tree);
            })
        }

    });

    // 按下ESC键就取消弹层
    $(document).on('keyup', function (e) {
        // 键码27为ESC键
        if (e.keyCode === 27) {
            // 关闭所有弹层
            layer.closeAll();
        }
    });

    //监听提交
    tree_form.on('submit(sbt)', function (data) {
        add(data.field);
        showTree(layui_tree);
        return false;
    });

});
